# README.md

## Credits

Art by

- Metball [ancientartonya](https://www.vecteezy.com/members/ancientartonya)
- Broccoli [sunshine-91](https://www.vecteezy.com/members/sunshine-91)
- Hand [Graphics RF](https://www.vecteezy.com/members/graphicsrf)
- Background [Graphics RF](https://www.vecteezy.com/members/graphicsrf)

## Online Project

- [v0.0.0](https://scratch.mit.edu/projects/530934799/)
- [v0.0.1](https://scratch.mit.edu/projects/530968332/) 👉 local multuplayer
- [v0.1.0](https://scratch.mit.edu/projects/535168352/) 👉 online multiplayer
